//
//  File.swift
//  Zazcar-iOS-Challenge
//
//  Created by Raphael on 27/03/17.
//  Copyright © 2017 raphzz. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

struct ApiHelper {
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL, iV: UIImageView) {
        getDataFromUrl(url: url) { (data, response, error)  in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() { () -> Void in
                iV.image = UIImage(data: data)
            }
        }
    }
    
    func getRepoPullRequests(ownerName: String, repoName: String, vc: Any) {
        Alamofire.request("https://api.github.com/repos/\(ownerName)/\(repoName)/pulls?state=all").responseJSON { response in
            
            if response.result.value != nil {
                let responseString = String(data: response.data!, encoding: .utf8)
                
                let data = responseString?.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as! [[String: AnyObject]]
                    
                    let pullResults = PullRequest.modelsFromDictionaryArray(array: json as NSArray)
                    
                    let destVc = vc as! RepoDetailsViewController
                    
                    destVc.displayResults(pulls: pullResults)
                    
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func searchRepoRequest(language: String, pageIndex: Int, vc: Any) {
        Alamofire.request("https://api.github.com/search/repositories?q=language:\(language)&sort=stars&page=\(pageIndex)").responseJSON { response in
            
            if response.result.value != nil {
                
                let responseString = String(data: response.data!, encoding: .utf8)
                
                let data = responseString?.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: AnyObject]
                    
                    let repoResults = Repository.modelsFromDictionaryArray(array: json["items"] as! NSArray)
                    
                    let destVc = vc as! MainViewController
                    
                    destVc.displayResults(repos: repoResults)
                    
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
            }
        }
    }
}
