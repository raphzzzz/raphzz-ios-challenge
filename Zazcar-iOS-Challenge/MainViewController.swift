//
//  ViewController.swift
//  Zazcar-iOS-Challenge
//
//  Created by Raphael on 27/03/17.
//  Copyright © 2017 raphzz. All rights reserved.
//

import UIKit
import Alamofire

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

struct Globals {
    public static var selectedRepo: Repository? = nil
}

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {

    @IBOutlet weak var repoTableView: UITableView!
    
    var pageIndex = 1;
    
    var repoResults = [Repository]()
    
    var isLoadingMore = false
    
    let apiHelper = ApiHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        apiHelper.searchRepoRequest(language: "Swift", pageIndex: pageIndex, vc: self)
        
        configureNavigationBar()
    }
    
    func configureNavigationBar() {
        navigationController?.navigationBar.barTintColor = UIColor.init(netHex: 0x343438)
    }
    
    func displayResults(repos: [Repository]) {
        repoTableView.isHidden = false
        
        for repo in repos {
            repoResults.append(repo)
        }
        
        repoTableView.delegate = self
        repoTableView.dataSource = self
        repoTableView.reloadData()
        
        isLoadingMore = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repoResults.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        Globals.selectedRepo = repoResults[indexPath.row]
        
        self.performSegue(withIdentifier: "show_details", sender: self)
        
        repoTableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        repoTableView.register(UINib(nibName: "RepoTableViewCell", bundle: nil), forCellReuseIdentifier: "repo_cell")
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "repo_cell", for: indexPath) as! RepoTableViewCell
        
        cell.repoNameLabel.text = repoResults[indexPath.row].name
        cell.repoDescrLabel.text = repoResults[indexPath.row].description
        cell.forksLabel.text = "\(repoResults[indexPath.row].forks!)"
        cell.favoritesLabel.text = "\(repoResults[indexPath.row].stargazers_count!)"
        
        if let checkedUrl = URL(string: (repoResults[indexPath.row].owner?.avatar_url)!) {
            cell.ownerAvatarImageView.contentMode = .scaleAspectFit
            apiHelper.downloadImage(url: checkedUrl, iV: cell.ownerAvatarImageView)
        }
        
        cell.ownerUsernameLabel.text = repoResults[indexPath.row].owner?.login
        cell.ownerFullnameLabel.text = repoResults[indexPath.row].full_name
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        // calculates where the user is in the y-axis
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if (offsetY > contentHeight - scrollView.frame.size.height && !isLoadingMore) {
            
            isLoadingMore = true
        
            pageIndex += 1
            
            apiHelper.searchRepoRequest(language: "Swift", pageIndex: pageIndex, vc: self)
        }
    }
}

