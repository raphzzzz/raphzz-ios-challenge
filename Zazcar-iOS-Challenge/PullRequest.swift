
import Foundation

public class PullRequest {
	public var title : String?
	public var user : User?
	public var body : String?
    public var state : String?
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [PullRequest]
    {
        var models:[PullRequest] = []
        for item in array
        {
            models.append(PullRequest(dictionary: item as! NSDictionary)!)
        }
        return models
    }

	required public init?(dictionary: NSDictionary) {

		title = dictionary["title"] as? String
		if (dictionary["user"] != nil) { user = User(dictionary: dictionary["user"] as! NSDictionary) }
		body = dictionary["body"] as? String
        state = dictionary["state"] as? String
	}

	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()
        
		dictionary.setValue(self.title, forKey: "title")
		dictionary.setValue(self.user?.dictionaryRepresentation(), forKey: "user")
		dictionary.setValue(self.body, forKey: "body")
        dictionary.setValue(self.state, forKey: "state")

		return dictionary
	}

}
