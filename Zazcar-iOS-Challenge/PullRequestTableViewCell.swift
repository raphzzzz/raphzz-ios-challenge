//
//  PullRequestTableViewCell.swift
//  Zazcar-iOS-Challenge
//
//  Created by Raphael on 27/03/17.
//  Copyright © 2017 raphzz. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var pullRequestNameLabel: UILabel!

    @IBOutlet weak var pullRequestDescrLabel: UILabel!
    
    @IBOutlet weak var ownerAvatarImageView: UIImageView!
    
    @IBOutlet weak var ownerUsernameLabel: UILabel!
    
    @IBOutlet weak var fullnameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
        self.ownerAvatarImageView.layer.cornerRadius = self.ownerAvatarImageView.frame.height/2
        self.ownerAvatarImageView.clipsToBounds = true
    }
}
