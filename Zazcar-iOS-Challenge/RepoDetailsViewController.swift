//
//  RepoDetailsViewController.swift
//  Zazcar-iOS-Challenge
//
//  Created by Raphael on 27/03/17.
//  Copyright © 2017 raphzz. All rights reserved.
//

import UIKit

class RepoDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var pullRequestsTableView: UITableView!
    
    @IBOutlet weak var openClosedLabel: UILabel!
    
    var openCount = 0
    
    var closedCount = 0
    
    let apiHelper = ApiHelper()
    
    var pullRequests = [PullRequest]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = Globals.selectedRepo?.name
        
        configureNavigationBar()
        
        apiHelper.getRepoPullRequests(ownerName: (Globals.selectedRepo?.owner?.login)!, repoName: (Globals.selectedRepo?.name)!, vc: self)
    }
    
    func displayResults(pulls: [PullRequest]) {
        
        for pull in pulls {
            if (pull.state == "open") {
                openCount += 1
            }else{
                closedCount += 1
            }
        }
        
        openClosedLabel.text = "\(openCount) open / \(closedCount) closed"
        
        pullRequests = pulls
        
        pullRequestsTableView.isHidden = false
        
        pullRequestsTableView.delegate = self
        pullRequestsTableView.dataSource = self
        pullRequestsTableView.reloadData()
    }
    
    func configureNavigationBar() {
        navigationController?.navigationBar.barTintColor = UIColor.init(netHex: 0x343438)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        pullRequestsTableView.register(UINib(nibName: "PullRequestTableViewCell", bundle: nil), forCellReuseIdentifier: "repo_details_cell")
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "repo_details_cell", for: indexPath) as! PullRequestTableViewCell
        
        cell.pullRequestNameLabel.text = pullRequests[indexPath.row].title
        cell.pullRequestDescrLabel.text = pullRequests[indexPath.row].body
        
        if let checkedUrl = URL(string: (pullRequests[indexPath.row].user?.avatar_url)!) {
            cell.ownerAvatarImageView.contentMode = .scaleAspectFit
            apiHelper.downloadImage(url: checkedUrl, iV: cell.ownerAvatarImageView)
        }
        
        cell.ownerUsernameLabel.text = pullRequests[indexPath.row].user?.login
        
        return cell
    }
}

