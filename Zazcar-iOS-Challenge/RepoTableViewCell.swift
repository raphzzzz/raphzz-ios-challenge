//
//  RepoTableViewCell.swift
//  Zazcar-iOS-Challenge
//
//  Created by Raphael on 27/03/17.
//  Copyright © 2017 raphzz. All rights reserved.
//

import UIKit

class RepoTableViewCell: UITableViewCell {

    @IBOutlet weak var repoNameLabel: UILabel!
    
    @IBOutlet weak var repoDescrLabel: UILabel!
    
    @IBOutlet weak var ownerAvatarImageView: UIImageView!
    
    @IBOutlet weak var ownerUsernameLabel: UILabel!
    
    @IBOutlet weak var ownerFullnameLabel: UILabel!
    
    @IBOutlet weak var forksLabel: UILabel!
    
    @IBOutlet weak var favoritesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.ownerAvatarImageView.layer.cornerRadius = self.ownerAvatarImageView.frame.height/2
        self.ownerAvatarImageView.clipsToBounds = true
    }
}
